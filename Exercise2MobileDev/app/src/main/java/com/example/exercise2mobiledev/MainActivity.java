package com.example.exercise2mobiledev;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    String text;
    TextView textView;
    public int counter = 0;
    String TAG = "My tag";
    int total=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        counter++;
        Log.i(TAG, "This is on create");

        //text views
        final TextView textViewLose = (TextView)findViewById(R.id.lose);
        final TextView textViewWin = (TextView)findViewById(R.id.win);
        final TextView textViewTotal = (TextView)findViewById(R.id.total);
        textViewLose.setTextColor(getColor(R.color.colorRed));
        textViewWin.setTextColor(getColor(R.color.colorGreen));
        textViewTotal.setTextColor(getColor(R.color.colorAccent));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        Button playButton = (Button) findViewById(R.id.Play);
        playButton.setOnClickListener(new View.OnClickListener() {


            public void onClick(View v) {
                long time= System.currentTimeMillis();
                total++;

                textViewTotal.setText("Clicks : " + total);
                if ((time%2) == 0){
                    textViewWin.setBackgroundColor(getColor(R.color.colorGreen));
                }
                else{
                    textViewLose.setBackgroundColor(getColor(R.color.colorRed));

                }
            }
        });
        Button resetButton = (Button) findViewById(R.id.Reset);
        resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Reset game in response to button click
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        counter++;
        Log.i(TAG, "This is on start");
    }

    @Override
    protected void onResume() {
        super.onResume();
        counter++;
        Log.i(TAG, "This is on resume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        counter++;
        Log.i(TAG, "This is on pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        counter++;
        Log.i(TAG, "This is on stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        counter++;
        Log.i(TAG, "This is on destroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
